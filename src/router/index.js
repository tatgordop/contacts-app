import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '../store'

import LogIn from '@/views/LogIn'
import Contacts from '@/views/Contacts'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Contacts
  },
  {
    path: '/login',
    name: 'Login',
    component: LogIn
  }

]


const router = new VueRouter({
  routes,
  mode: 'history',
})

router.beforeEach((to, from, next) => {
  if (to.fullPath !== '/login' && !store.getters.isLoggedIn) next('/login')
  else if (to.fullPath === '/login' && store.getters.isLoggedIn) next('/')
  else next()
})

export default router
