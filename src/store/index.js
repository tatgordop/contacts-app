import Vue from 'vue'
import Vuex from 'vuex'

import httpClient from '../axios';

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    token: localStorage.getItem('user-token') || '',
    contacts: []
  },
  getters: {
    getContacts(state) {
      return state.contacts;
    },
    isLoggedIn (state) {
      return !!state.token
    }
  },

  mutations: {
    setContactsList(state, data) {
      state.contacts = data;
    },
    deleteContact(state, id) {
      let index = state.contactsList.findIndex(element => element.id === id);
      state.contactsList.splice(index, 1);
    },
    setToken(state, token) {
      state.token = token;
    }

  },
  actions: {
    login: ({ commit }, { email, password }) => {
      return new Promise((resolve, reject) => {
        httpClient.post('/auth/login', { email, password })
          .then(response => {
            const token = response.data.access_token
            commit('setToken', token)
            localStorage.setItem('user-token', token)
            resolve(response)
          })
          .catch(err => {
            localStorage.removeItem('user-token')
            reject(err)
          })
      })
    },

    logout: ({ commit }) => {
      return new Promise((resolve) => {
        commit('setToken', '')
        localStorage.removeItem('user-token')
        resolve()
      })
    },

    fetchContacts: ({ commit }) => {
      return new Promise((resolve, reject) => {
        httpClient.get('/contacts')
          .then(response => {
            commit('setContactsList', response.data.reverse())
            resolve(response)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    deleteContactItem: ({ dispatch }, id) => {
      return new Promise((resolve, reject) => {
        httpClient.delete(`/contacts/${id}`)
          .then((response) => {
            dispatch('fetchContacts');
            resolve(response)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    createContactItem: ({ dispatch }, { name, email, phone }) => {
      return new Promise((resolve, reject) => {
        httpClient.post(`/contacts`, { name, email, phone })
          .then((response) => {
            dispatch('fetchContacts');
            resolve(response)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    changeContactItem: ({ dispatch }, [id, { name, email, phone }]) => {
      return new Promise((resolve, reject) => {
        httpClient.put(`/contacts/${id}`, { name, email, phone })
          .then((response) => {
            dispatch('fetchContacts');
            resolve(response)
          })
          .catch(err => {
            reject(err)
          })
      })
    }

  },
  modules: {
  }
})
