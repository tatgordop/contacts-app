
import Vue from 'vue';
import axios from 'axios';
import router from '../router'
import store from '../store'

const httpClient = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  }
});


httpClient.interceptors.request.use(function (config) {
  const token = localStorage.getItem('user-token');
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  } else {
    delete config.headers.authorization;
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});

httpClient.interceptors.response.use(function (response) {
  return response
}, function (error) {

  if (!error.response) {
    Vue.notify({
      type: "error",
      title: `Server Error`,
    });
    return Promise.reject(error);
  }

  if (error.response.status === 401) {
    store.dispatch('logout')

    if (router.currentRoute.fullPath !== '/login') {
      router.push('/login')
    }
  }

  Vue.notify({
    type: "error",
    title: `Error: ${error.response.data.status}`,
    text: `${error.response.data.message}`
  });

  return Promise.reject(error)
})

export default httpClient;